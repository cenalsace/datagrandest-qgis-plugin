# -*- coding: utf-8 -*-

import os.path

from datagrandest.gui.about_box import AboutBox
from datagrandest.gui.dock import DockWidget
from datagrandest.gui.param_box import ParamBox
from datagrandest.nodes.tree_node_factory import (
    TreeNodeFactory,
    download_tree_config_file,
)
from datagrandest.utils.plugin_globals import PluginGlobals
from datagrandest.utils.plugin_icons import PluginIcons
from qgis.core import QgsProject
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QAction, QDockWidget, QMenu, QToolBar


class SimpleAccessPlugin:
    """
    Plugin initialisation.
    A json config file is read in order to configure the plugin GUI.
    """

    def __init__(self, iface):
        self.iface = iface
        self.dock = None

        PluginGlobals.instance().set_plugin_path(
            os.path.dirname(os.path.abspath(__file__))
        )
        PluginGlobals.instance().set_plugin_iface(self.iface)
        PluginGlobals.instance().reload_globals_from_qgis_settings()

        config_struct = None
        config_string = ""

        # Download the config if needed
        if self.need_download_tree_config_file():
            download_tree_config_file(PluginGlobals.instance().CONFIG_FILE_URLS[0])

        # Read the resources tree file and update the GUI
        self.ressources_tree = TreeNodeFactory(
            PluginGlobals.instance().config_file_path
        ).root_node

    def need_download_tree_config_file(self):
        """
        Do we need to download a new version of the resources tree file?
        2 possible reasons:
        - the user wants it to be downloading at plugin start up
        - the file is currently missing
        """

        return (
            PluginGlobals.instance().CONFIG_FILES_DOWNLOAD_AT_STARTUP > 0
            or not os.path.isfile(PluginGlobals.instance().config_file_path)
        )

    def initGui(self):
        """
        Plugin GUI initialisation.
        Creates a menu item in the menu of QGIS
        Creates a DockWidget containing the tree of resources
        """

        # Create a menu
        self.createPluginMenu()

        # Create a dockable panel with a tree of resources
        self.dock = DockWidget()
        self.dock.set_tree_content(self.ressources_tree)
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dock)
        # self.dock.hide()
        QgsProject.instance().layersAdded.connect(self.load_layer_style)

    def createPluginMenu(self):
        """
        Creates the plugin main menu
        """
        plugin_menu = self.iface.pluginMenu()
        self.plugin_menu = QMenu("DataGrandEst", plugin_menu)
        plugin_menu.addMenu(self.plugin_menu)

        plugin_icons = PluginIcons.instance()
        show_panel_action = QAction(
            plugin_icons.dge_logo_icon,
            "Afficher le panneau latéral",
            self.iface.mainWindow(),
        )
        show_panel_action.triggered.connect(self.showPanelMenuTriggered)
        self.plugin_menu.addAction(show_panel_action)

        param_action = QAction("Paramétrer le plugin…", self.iface.mainWindow())
        param_action.triggered.connect(self.paramMenuTriggered)
        self.plugin_menu.addAction(param_action)

        about_action = QAction("À propos…", self.iface.mainWindow())
        about_action.triggered.connect(self.aboutMenuTriggered)
        self.plugin_menu.addAction(about_action)

        layers_dock = self.iface.mainWindow().findChild(QDockWidget, "Layers")
        toolbar = layers_dock.findChildren(QToolBar)[0]
        toolbar.insertAction(toolbar.actions()[-1], show_panel_action)
        toolbar.insertAction(show_panel_action, toolbar.actions()[-1])

    def showPanelMenuTriggered(self):
        """
        Shows the dock widget
        """
        if self.dock.isVisible():
            self.dock.hide()
        else:
            self.dock.show()

    def aboutMenuTriggered(self):
        """
        Shows the About box
        """
        dialog = AboutBox(self.iface.mainWindow())
        dialog.exec_()

    def paramMenuTriggered(self):
        """
        Shows the Param box
        """
        dialog = ParamBox(self.iface.mainWindow(), self.dock)
        dialog.exec_()

    def load_layer_style(self, layers):
        if getattr(QgsProject.instance(), "dge_custom_vars", None) is None:
            QgsProject.instance().dge_custom_vars = {"styles": {}}

        dge_custom_vars = getattr(QgsProject.instance(), "dge_custom_vars")

        lyr = layers[0]
        if lyr.name() in dge_custom_vars["styles"]:
            lyr.loadNamedStyle(
                f'{PluginGlobals.instance().style_dir_path}{os.sep}{dge_custom_vars["styles"][lyr.name()]}'
            )
            lyr.triggerRepaint()

    def unload(self):
        """
        Removes the plugin menu
        """
        self.iface.pluginMenu().removeAction(self.plugin_menu.menuAction())

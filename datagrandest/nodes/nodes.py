# -*- coding: utf-8 -*-

import os
from datetime import datetime

from datagrandest.utils.plugin_globals import PluginGlobals
from datagrandest.utils.plugin_icons import PluginIcons
from qgis.core import (
    QgsDataSourceUri,
    QgsExpressionContextUtils,
    QgsProject,
    QgsSettings,
)


class FavoritesTreeNode:
    """ """

    def _add_credits(run_add_to_map_action):
        def wrapper(self):
            lyr = run_add_to_map_action(self)
            if lyr:
                # Retrieve layer and set metadata
                lyr_meta = lyr.metadata()
                lyr_meta.setRights(self.credits)
                lyr.setMetadata(lyr_meta)

            return lyr

        return wrapper

    def _load_style(run_add_to_map_action):
        def wrapper(self, *args, **kwargs):
            if getattr(QgsProject.instance(), "dge_custom_vars", None) is None:
                QgsProject.instance().dge_custom_vars = {"styles": {}}

            dge_custom_vars = getattr(QgsProject.instance(), "dge_custom_vars")
            if self.style:
                if "style" in kwargs:
                    selected_style = self.style[kwargs["style"]]
                else:
                    selected_style = self.style[list(self.style)[0]]

                dge_custom_vars["styles"][self.title] = selected_style

            lyr = run_add_to_map_action(self)
            return lyr

        return wrapper

    def __init__(
        self,
        title,
        node_type=PluginGlobals.instance().NODE_TYPE_FOLDER,
        description=None,
        status=None,
        metadata_url=None,
        ident=None,
        params=None,
        bounding_boxes=None,
        style=None,
        credits=None,
        parent_node=None,
    ):
        """ """

        self.parent_node = parent_node
        self.node_type = node_type
        self.title = title
        self.description = description
        self.status = status
        self.metadata_url = metadata_url
        self.ident = ident
        self.bounding_boxes = bounding_boxes
        self.children = []
        self.can_be_added_to_map = False
        self.icon = None

        self.style = style
        credits = ["⛔ sour©es manquantes ❗"] if credits is None else credits
        year = datetime.now().strftime("%Y")
        self.credits = [c.format(year=year) for c in credits]

    def layer_mime_data(self):
        """
        Return the mime data used by the drag and drop process
        and needed by QGIS to add the right layer to the map
        """

        qgis_layer_details = self.get_qgis_layer_details()
        mime_data = ":".join(
            [
                qgis_layer_details["type"],
                qgis_layer_details["provider"],
                qgis_layer_details["title"].replace(":", "\\:"),
                qgis_layer_details["uri"].replace(":", "\\:"),
            ]
        )

        return mime_data

    def run_add_to_map_action(self):
        """ """

        pass

    def run_show_metadata_action(self):
        """
        Opens in the default user web browser the web page displaying the resource metadata
        """

        import webbrowser

        if self.metadata_url:
            webbrowser.open_new_tab(self.metadata_url)

    def run_report_issue_action(self):
        """
        Opens the default mail client to let the user send an issue report by email
        """

        import webbrowser

        user_full_name = QgsExpressionContextUtils.globalScope().variable(
            "user_full_name"
        )
        line_break = "%0D%0A"
        mail_body = (
            f"Bonjour,{line_break}"
            f"J'ai des difficultés / des questions à propros de la couche : {self.description}.{line_break}"
            f"{line_break}"
            f"- [ ] les données ne s'affichent pas{line_break}"
            f"- [ ] les données ne sont pas à jour{line_break}"
            f"- [ ] je n'ai pas trouvé ces données :{line_break}"
            f"- [ ] J'aimerai proposer un style ci-joint à ce mail{line_break}"
            f"- [ ] Autre, préciser :{line_break}"
            f"{line_break}"
            f"Bonne journée,{line_break}"
            f"{user_full_name}{line_break}"
        )

        webbrowser.open(
            f"mailto:{PluginGlobals.instance().PLUGIN_MAINTAINER}?subject=[Données SIG] Questions à propos de la couche : {self.title}&body={mail_body}"
        )


# Subclasses of FavoritesTreeNode


class FolderTreeNode(FavoritesTreeNode):
    """
    Tree node for a folder containing other nodes
    """

    def __init__(
        self,
        title,
        node_type=PluginGlobals.instance().NODE_TYPE_FOLDER,
        description=None,
        status=None,
        metadata_url=None,
        ident=None,
        params=None,
        bounding_boxes=None,
        style=None,
        credits=None,
        parent_node=None,
    ):
        """ """

        FavoritesTreeNode.__init__(
            self,
            title,
            node_type,
            description,
            status,
            metadata_url,
            ident,
            params,
            bounding_boxes,
            style,
            credits,
            parent_node,
        )

        # Icon
        plugin_icons = PluginIcons.instance()
        self.icon = plugin_icons.folder_icon
        if self.status == PluginGlobals.instance().NODE_STATUS_WARN:
            self.icon = plugin_icons.warn_icon


class WmsLayerTreeNode(FavoritesTreeNode):
    """
    Tree node for a WMS layer
    """

    def __init__(
        self,
        title,
        node_type=PluginGlobals.instance().NODE_TYPE_WMS_LAYER,
        description=None,
        status=None,
        metadata_url=None,
        ident=None,
        params=None,
        bounding_boxes=None,
        style=None,
        credits=None,
        parent_node=None,
    ):
        """ """
        FavoritesTreeNode.__init__(
            self,
            title,
            node_type,
            description,
            status,
            metadata_url,
            ident,
            params,
            bounding_boxes,
            style,
            credits,
            parent_node,
        )

        self.service_url = params.get("url")
        self.layer_name = params.get("name")
        self.layer_format = params.get("format")
        self.layer_srs = params.get("srs")
        self.layer_style_name = params.get("style", "")
        self.can_be_added_to_map = True

        # Icon
        plugin_icons = PluginIcons.instance()
        self.icon = plugin_icons.wms_layer_icon
        if self.status == PluginGlobals.instance().NODE_STATUS_WARN:
            self.icon = plugin_icons.warn_icon

    def get_qgis_layer_details(self):
        """
        Return the details of the layer used by QGIS to add the layer to the map.
        This dictionary is used by the run_add_to_map_action and layerMimeData methods.
        """
        qgis_layer_uri_details = {
            "type": "raster",
            "provider": "wms",
            "title": self.title,
            "uri": "crs={}&featureCount=10&format={}&layers={}&maxHeight=256&maxWidth=256&styles={}&url={}".format(
                self.layer_srs,
                self.layer_format,
                self.layer_name,
                self.layer_style_name,
                self.service_url,
            ),
        }

        return qgis_layer_uri_details

    @FavoritesTreeNode._add_credits
    def run_add_to_map_action(self):
        """
        Add the WMS layer with the specified style to the map
        """
        qgis_layer_details = self.get_qgis_layer_details()
        return PluginGlobals.instance().iface.addRasterLayer(
            qgis_layer_details["uri"],
            qgis_layer_details["title"],
            qgis_layer_details["provider"],
        )


class WmsStyleLayerTreeNode(FavoritesTreeNode):
    """
    Tree node for a WMS layer with a specific style
    """

    def __init__(
        self,
        title,
        node_type=PluginGlobals.instance().NODE_TYPE_WMS_LAYER_STYLE,
        description=None,
        status=None,
        metadata_url=None,
        ident=None,
        params=None,
        bounding_boxes=None,
        style=None,
        credits=None,
        parent_node=None,
    ):
        """ """

        FavoritesTreeNode.__init__(
            self,
            title,
            node_type,
            description,
            status,
            metadata_url,
            ident,
            params,
            bounding_boxes,
            style,
            credits,
            parent_node,
        )

        self.layer_style_name = params.get("name")
        self.can_be_added_to_map = True

        # Icon
        plugin_icons = PluginIcons.instance()
        self.icon = plugin_icons.wms_style_icon
        if self.status == PluginGlobals.instance().NODE_STATUS_WARN:
            self.icon = plugin_icons.warn_icon

    def get_qgis_layer_details(self):
        """
        Return the details of the layer used by QGIS to add the layer to the map.
        This dictionary is used by the run_add_to_map_action and layerMimeData methods.
        """
        if self.parent_node is None:
            return None

        qgis_layer_uri_details = {
            "type": "raster",
            "provider": "wms",
            "title": self.title,
            "uri": "crs={}&featureCount=10&format={}&layers={}&maxHeight=256&maxWidth=256&styles={}&url={}".format(
                self.parent_node.layer_srs,
                self.parent_node.layer_format,
                self.parent_node.layer_name,
                self.layer_style_name,
                self.parent_node.service_url,
            ),
        }

        return qgis_layer_uri_details

    @FavoritesTreeNode._add_credits
    def run_add_to_map_action(self):
        """
        Add the WMS layer with the specified style to the map
        """

        qgis_layer_details = self.get_qgis_layer_details()
        if qgis_layer_details is not None:
            return PluginGlobals.instance().iface.addRasterLayer(
                qgis_layer_details["uri"],
                qgis_layer_details["title"],
                qgis_layer_details["provider"],
            )


class WmtsLayerTreeNode(FavoritesTreeNode):
    """
    Tree node for a WMTS layer
    """

    def __init__(
        self,
        title,
        node_type=PluginGlobals.instance().NODE_TYPE_WMTS_LAYER,
        description=None,
        status=None,
        metadata_url=None,
        ident=None,
        params=None,
        bounding_boxes=None,
        style=None,
        credits=None,
        parent_node=None,
    ):
        """ """
        FavoritesTreeNode.__init__(
            self,
            title,
            node_type,
            description,
            status,
            metadata_url,
            ident,
            params,
            bounding_boxes,
            style,
            credits,
            parent_node,
        )

        self.service_url = params.get("url")
        self.layer_tilematrixset_name = params.get("tilematrixset_name")
        self.layer_name = params.get("name")
        self.layer_format = params.get("format")
        self.layer_srs = params.get("srs")
        self.layer_style_name = params.get("style", "")
        self.can_be_added_to_map = True

        # Icon
        plugin_icons = PluginIcons.instance()
        self.icon = plugin_icons.wms_style_icon
        if self.status == PluginGlobals.instance().NODE_STATUS_WARN:
            self.icon = plugin_icons.warn_icon

    def get_qgis_layer_details(self):
        """
        Return the details of the layer used by QGIS to add the layer to the map.
        This dictionary is used by the run_add_to_map_action and layerMimeData methods.
        """
        qgis_layer_uri_details = {
            "type": "raster",
            "provider": "wms",
            "title": self.title,
            "uri": "tileMatrixSet={}&crs={}&featureCount=10&format={}&layers={}&maxHeight=256&maxWidth=256&styles={}&url={}".format(
                self.layer_tilematrixset_name,
                self.layer_srs,
                self.layer_format,
                self.layer_name,
                self.layer_style_name,
                self.service_url,
            ),
        }

        return qgis_layer_uri_details

    @FavoritesTreeNode._add_credits
    def run_add_to_map_action(self):
        """
        Add the WMTS layer to the map
        """
        qgis_layer_details = self.get_qgis_layer_details()
        if qgis_layer_details is not None:
            return PluginGlobals.instance().iface.addRasterLayer(
                qgis_layer_details["uri"],
                qgis_layer_details["title"],
                qgis_layer_details["provider"],
            )


class WfsFeatureTypeTreeNode(FavoritesTreeNode):
    """
    Tree node for a WFS feature type
    """

    def __init__(
        self,
        title,
        node_type=PluginGlobals.instance().NODE_TYPE_WFS_FEATURE_TYPE,
        description=None,
        status=None,
        metadata_url=None,
        ident=None,
        params=None,
        bounding_boxes=None,
        style=None,
        credits=None,
        parent_node=None,
    ):
        """ """
        FavoritesTreeNode.__init__(
            self,
            title,
            node_type,
            description,
            status,
            metadata_url,
            ident,
            params,
            bounding_boxes,
            style,
            credits,
            parent_node,
        )

        self.service_url = params.get("url")
        self.feature_type_name = params.get("name")
        self.filter = params.get("filter")
        self.wfs_version = params.get("version", "1.0.0")
        self.layer_srs = params.get("srs")
        self.can_be_added_to_map = True

        # Icon
        plugin_icons = PluginIcons.instance()
        self.icon = plugin_icons.wfs_layer_icon
        if self.status == PluginGlobals.instance().NODE_STATUS_WARN:
            self.icon = plugin_icons.warn_icon

    def get_qgis_layer_details(self):
        """
        Return the details of the layer used by QGIS to add the layer to the map.
        This dictionary is used by the run_add_to_map_action and layerMimeData methods.
        """

        params = {
            "pagingEnabled": "true",
            "preferCoordinatesForWfsT11": "false",
            "url": self.service_url,
            "typename": self.feature_type_name,
            "srsname": self.layer_srs,
            "version": self.wfs_version,
        }

        uri = QgsDataSourceUri()
        for k, v in params.items():
            uri.setParam(k, v)

        if self.filter:
            uri.setSql(self.filter)

        qgis_layer_uri_details = {
            "type": "vector",
            "provider": "WFS",
            "title": self.title,
            "uri": uri.uri(),
        }

        return qgis_layer_uri_details

    @FavoritesTreeNode._load_style
    @FavoritesTreeNode._add_credits
    def run_add_to_map_action(self):
        """
        Add the WFS feature type to the map
        """
        qgis_layer_details = self.get_qgis_layer_details()
        if qgis_layer_details is not None:
            return PluginGlobals.instance().iface.addVectorLayer(
                qgis_layer_details["uri"],
                qgis_layer_details["title"],
                qgis_layer_details["provider"],
            )


class WfsFeatureTypeFilterTreeNode(FavoritesTreeNode):
    """
    Tree node for a WFS feature type filter
    """

    def __init__(
        self,
        title,
        node_type=PluginGlobals.instance().NODE_TYPE_WFS_FEATURE_TYPE_FILTER,
        description=None,
        status=None,
        metadata_url=None,
        ident=None,
        params=None,
        bounding_boxes=None,
        style=None,
        credits=None,
        parent_node=None,
    ):
        """ """
        FavoritesTreeNode.__init__(
            self,
            title,
            node_type,
            description,
            status,
            metadata_url,
            ident,
            params,
            bounding_boxes,
            style,
            credits,
            parent_node,
        )

        self.filter = params.get("filter")
        self.can_be_added_to_map = True

        # Icon
        plugin_icons = PluginIcons.instance()
        self.icon = plugin_icons.wfs_layer_icon
        if self.status == PluginGlobals.instance().NODE_STATUS_WARN:
            self.icon = plugin_icons.warn_icon

    def get_qgis_layer_details(self):
        """
        Return the details of the layer used by QGIS to add the layer to the map.
        This dictionary is used by the run_add_to_map_action and layerMimeData methods.
        """

        if self.parent_node is None:
            return None

        first_param_prefix = "?"
        if "?" in self.parent_node.service_url:
            first_param_prefix = "&"

        uri = "{}{}SERVICE=WFS&VERSION={}&REQUEST=GetFeature&TYPENAME={}&SRSNAME={}".format(
            self.parent_node.service_url,
            first_param_prefix,
            self.parent_node.wfs_version,
            self.parent_node.feature_type_name,
            self.parent_node.layer_srs,
        )

        if self.filter:
            uri += "&Filter={}".format(self.filter)

        qgis_layer_uri_details = {
            "type": "vector",
            "provider": "WFS",
            "title": self.title,
            "uri": uri,
        }

        return qgis_layer_uri_details

    @FavoritesTreeNode._load_style
    @FavoritesTreeNode._add_credits
    def run_add_to_map_action(self):
        """
        Add the WFS feature type to the map with a filter
        """

        qgis_layer_details = self.get_qgis_layer_details()
        if qgis_layer_details is not None:
            return PluginGlobals.instance().iface.addVectorLayer(
                qgis_layer_details["uri"],
                qgis_layer_details["title"],
                qgis_layer_details["provider"],
            )


class GdalWmsConfigFileTreeNode(FavoritesTreeNode):
    """
    Tree node for a GDAL WMS config file
    """

    def __init__(
        self,
        title,
        node_type=PluginGlobals.instance().NODE_TYPE_GDAL_WMS_CONFIG_FILE,
        description=None,
        status=None,
        metadata_url=None,
        ident=None,
        params=None,
        bounding_boxes=None,
        style=None,
        credits=None,
        parent_node=None,
    ):
        """ """
        FavoritesTreeNode.__init__(
            self,
            title,
            node_type,
            description,
            status,
            metadata_url,
            ident,
            params,
            bounding_boxes,
            style,
            credits,
            parent_node,
        )

        self.gdal_config_file_path = os.path.join(
            PluginGlobals.instance().config_dir_path, params.get("file_path")
        )
        self.can_be_added_to_map = True

        # Icon
        plugin_icons = PluginIcons.instance()
        self.icon = plugin_icons.raster_layer_icon
        if self.status == PluginGlobals.instance().NODE_STATUS_WARN:
            self.icon = plugin_icons.warn_icon

    def get_qgis_layer_details(self):
        """
        Return the details of the layer used by QGIS to add the layer to the map.
        This dictionary is used by the run_add_to_map_action and layerMimeData methods.
        """
        qgis_layer_uri_details = {
            "type": "raster",
            "provider": "gdal",
            "title": self.title,
            "uri": self.gdal_config_file_path.replace("\\", "/"),
        }

        return qgis_layer_uri_details

    @FavoritesTreeNode._add_credits
    def run_add_to_map_action(self):
        """
        Add the preconfigured TMS layer to the map
        """
        # PluginGlobals.instance().iface.addRasterLayer(self.gdal_config_file_path, self.title)
        qgis_layer_details = self.get_qgis_layer_details()
        if qgis_layer_details is not None:
            return PluginGlobals.instance().iface.addRasterLayer(
                qgis_layer_details["uri"], qgis_layer_details["title"]
            )


class PostgisLayerTreeNode(FavoritesTreeNode):
    """
    Tree node for a PostGIS layer
    """

    def __init__(
        self,
        title,
        node_type=PluginGlobals.instance().NODE_TYPE_POSTGIS,
        description=None,
        status=None,
        metadata_url=None,
        ident=None,
        params=None,
        bounding_boxes=None,
        style=None,
        credits=None,
        parent_node=None,
    ):
        """ """
        FavoritesTreeNode.__init__(
            self,
            title,
            node_type,
            description,
            status,
            metadata_url,
            ident,
            params,
            bounding_boxes,
            style,
            credits,
            parent_node,
        )

        self.connection_name = params.get("connection")
        self.schema_name = params.get("schema")
        self.table_name = params.get("table")
        self.key_column = params.get("key_column")
        self.geometry_column = params.get("geometry_column")
        self.filter = params.get("filter")
        self.can_be_added_to_map = True

        # Icon
        plugin_icons = PluginIcons.instance()
        self.icon = plugin_icons.postgis_layer_icon
        if self.status == PluginGlobals.instance().NODE_STATUS_WARN:
            self.icon = plugin_icons.warn_icon

    def get_qgis_layer_details(self):
        """
        Return the details of the layer used by QGIS to add the layer to the map.
        This dictionary is used by the run_add_to_map_action and layerMimeData methods.
        """
        params = QgsSettings()
        params.beginGroup(f"/PostgreSQL/connections/{self.connection_name}")
        cfg = {}
        cfg["aDatabase"] = params.value("database")
        cfg["aHost"] = params.value("host")
        cfg["aPort"] = params.value("port")
        cfg["authConfigId"] = params.value("authcfg")
        cfg["aUsername"] = None
        cfg["aPassword"] = None

        uri = QgsDataSourceUri()
        uri.setConnection(**cfg)
        uri.setDataSource(
            self.schema_name, self.table_name, self.geometry_column, self.filter
        )
        uri.setKeyColumn(self.key_column)

        qgis_layer_uri_details = {
            "type": "vector",
            "provider": "postgres",
            "title": self.title,
            "uri": uri.uri(False),
        }

        return qgis_layer_uri_details

    @FavoritesTreeNode._load_style
    @FavoritesTreeNode._add_credits
    def run_add_to_map_action(self, style: str = ""):
        """
        Add the PostGIS layer to the map
        """
        qgis_layer_details = self.get_qgis_layer_details()
        if qgis_layer_details is not None:
            return PluginGlobals.instance().iface.addVectorLayer(
                qgis_layer_details["uri"],
                qgis_layer_details["title"],
                qgis_layer_details["provider"],
            )

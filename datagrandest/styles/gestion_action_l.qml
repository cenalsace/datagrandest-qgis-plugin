<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.28.3-Firenze" styleCategories="Fields|Forms">
  <fieldConfiguration>
    <field configurationFlags="None" name="id">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="pg_user">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="{2839923C-8B7D-419E-B84B-CA2FE9B80EC7}" name="&lt;NULL>" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="alessandro.lani" name="Alessandro LANI" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="annaelle.muller" name="Annaëlle MULLER" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="delphine.schlaeflin" name="Delphine SCHLAEFLIN" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="elisa.schorr" name="Elisa SCHORR" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="gilles.grunenwald" name="Gilles GRUNENWALD" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="laura.grandadam" name="Laura GRANDADAM" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="louis.thiebaut" name="Louis THIEBAUT" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="luc.dietrich" name="Luc DIETRICH" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="luna.ghelab" name="Luna GHELAB" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="may-li.batot" name="May-Li BÂTOT" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="michael.moock" name="Michaël MOOCK" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="muriel.diss-schott" name="Muriel DISS-SCHOTT" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="pierre.goertz" name="Pierre GOERTZ" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="richard.peter" name="Richard PETER" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="victor.schoenfelder" name="Victor SCHOENFELDER" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="victoria.michel" name="Victoria MICHEL" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="vincent.wolf" name="Vincent WOLF" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="type">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="fauche" name="Fauche" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="pâturage" name="Patûrage" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="étrépage" name="Étrépage" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="andain" name="Andain" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_debut">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="dd/MM/yyyy" name="display_format" type="QString"/>
            <Option value="dd/MM/yyyy" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="date_fin">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="dd/MM/yyyy" name="display_format" type="QString"/>
            <Option value="dd/MM/yyyy" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="contact_prestataire">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="prestation_remuneree">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="paturage_nb_jours">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="360" name="Max" type="int"/>
            <Option value="0" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="paturage_nb_ugb">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="100" name="Max" type="double"/>
            <Option value="0" name="Min" type="double"/>
            <Option value="2" name="Precision" type="int"/>
            <Option value="0.25" name="Step" type="double"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="commentaire">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="true" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="code_site">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="nom_site">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="insee_com">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="commune">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="geom_p">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="geom_s">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name=""/>
    <alias field="pg_user" index="1" name="Utilisateur"/>
    <alias field="type" index="2" name="Type d'action"/>
    <alias field="date_debut" index="3" name="Date de début"/>
    <alias field="date_fin" index="4" name="Date de fin"/>
    <alias field="contact_prestataire" index="5" name="Contact du prestataire"/>
    <alias field="prestation_remuneree" index="6" name="Préstation rémunérée"/>
    <alias field="paturage_nb_jours" index="7" name="Nb jours de pâturage"/>
    <alias field="paturage_nb_ugb" index="8" name="Nb UGB"/>
    <alias field="commentaire" index="9" name="Commentaire"/>
    <alias field="code_site" index="10" name=""/>
    <alias field="nom_site" index="11" name="Nom du site"/>
    <alias field="insee_com" index="12" name=""/>
    <alias field="commune" index="13" name="Commune"/>
    <alias field="geom_p" index="14" name=""/>
    <alias field="geom_s" index="15" name=""/>
  </aliases>
  <defaults>
    <default field="id" applyOnUpdate="0" expression="-1"/>
    <default field="pg_user" applyOnUpdate="0" expression=""/>
    <default field="type" applyOnUpdate="0" expression=""/>
    <default field="date_debut" applyOnUpdate="0" expression=""/>
    <default field="date_fin" applyOnUpdate="0" expression=""/>
    <default field="contact_prestataire" applyOnUpdate="0" expression=""/>
    <default field="prestation_remuneree" applyOnUpdate="0" expression=""/>
    <default field="paturage_nb_jours" applyOnUpdate="0" expression=""/>
    <default field="paturage_nb_ugb" applyOnUpdate="0" expression=""/>
    <default field="commentaire" applyOnUpdate="0" expression=""/>
    <default field="code_site" applyOnUpdate="0" expression=""/>
    <default field="nom_site" applyOnUpdate="0" expression=""/>
    <default field="insee_com" applyOnUpdate="0" expression=""/>
    <default field="commune" applyOnUpdate="0" expression=""/>
    <default field="geom_p" applyOnUpdate="0" expression=""/>
    <default field="geom_s" applyOnUpdate="0" expression=""/>
  </defaults>
  <constraints>
    <constraint constraints="3" field="id" notnull_strength="1" unique_strength="1" exp_strength="0"/>
    <constraint constraints="0" field="pg_user" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="type" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="date_debut" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="date_fin" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="contact_prestataire" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="prestation_remuneree" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="paturage_nb_jours" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="paturage_nb_ugb" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="commentaire" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="code_site" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="nom_site" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="insee_com" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="commune" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="geom_p" notnull_strength="0" unique_strength="0" exp_strength="0"/>
    <constraint constraints="0" field="geom_s" notnull_strength="0" unique_strength="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" desc="" exp=""/>
    <constraint field="pg_user" desc="" exp=""/>
    <constraint field="type" desc="" exp=""/>
    <constraint field="date_debut" desc="" exp=""/>
    <constraint field="date_fin" desc="" exp=""/>
    <constraint field="contact_prestataire" desc="" exp=""/>
    <constraint field="prestation_remuneree" desc="" exp=""/>
    <constraint field="paturage_nb_jours" desc="" exp=""/>
    <constraint field="paturage_nb_ugb" desc="" exp=""/>
    <constraint field="commentaire" desc="" exp=""/>
    <constraint field="code_site" desc="" exp=""/>
    <constraint field="nom_site" desc="" exp=""/>
    <constraint field="insee_com" desc="" exp=""/>
    <constraint field="commune" desc="" exp=""/>
    <constraint field="geom_p" desc="" exp=""/>
    <constraint field="geom_s" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui est appelée lorsque le formulaire est
ouvert.

Utilisez cette fonction pour ajouter une logique supplémentaire à vos formulaires.

Entrez le nom de la fonction dans le champ 
"Fonction d'initialisation Python".
Voici un exemple:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
      <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
    </labelStyle>
    <attributeEditorField showLabel="0" index="0" name="id">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="1" name="pg_user">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="2" name="type">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="3" name="date_debut">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="4" name="date_fin">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="5" name="contact_prestataire">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="6" name="prestation_remuneree">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="7" name="paturage_nb_jours">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="8" name="paturage_nb_ugb">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="9" name="commentaire">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="0" index="10" name="code_site">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="11" name="nom_site">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="0" index="12" name="insee_com">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="1" index="13" name="commune">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="0" index="14" name="geom_p">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField showLabel="0" index="15" name="geom_s">
      <labelStyle labelColor="0,0,0,255" overrideLabelFont="0" overrideLabelColor="0">
        <labelFont description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" bold="0" strikethrough="0" style="" underline="0" italic="0"/>
      </labelStyle>
    </attributeEditorField>
  </attributeEditorForm>
  <editable>
    <field name="code_site" editable="0"/>
    <field name="commentaire" editable="1"/>
    <field name="commune" editable="0"/>
    <field name="contact_prestataire" editable="1"/>
    <field name="date_debut" editable="1"/>
    <field name="date_fin" editable="1"/>
    <field name="geom_l" editable="0"/>
    <field name="geom_p" editable="0"/>
    <field name="geom_s" editable="0"/>
    <field name="id" editable="0"/>
    <field name="insee_com" editable="0"/>
    <field name="nom_site" editable="0"/>
    <field name="paturage_nb_jours" editable="1"/>
    <field name="paturage_nb_ugb" editable="1"/>
    <field name="pg_user" editable="1"/>
    <field name="prestation_remuneree" editable="1"/>
    <field name="type" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="code_site"/>
    <field labelOnTop="0" name="commentaire"/>
    <field labelOnTop="0" name="commune"/>
    <field labelOnTop="0" name="contact_prestataire"/>
    <field labelOnTop="0" name="date_debut"/>
    <field labelOnTop="0" name="date_fin"/>
    <field labelOnTop="0" name="geom_l"/>
    <field labelOnTop="0" name="geom_p"/>
    <field labelOnTop="0" name="geom_s"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="insee_com"/>
    <field labelOnTop="0" name="nom_site"/>
    <field labelOnTop="0" name="paturage_nb_jours"/>
    <field labelOnTop="0" name="paturage_nb_ugb"/>
    <field labelOnTop="0" name="pg_user"/>
    <field labelOnTop="0" name="prestation_remuneree"/>
    <field labelOnTop="0" name="type"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="code_site"/>
    <field reuseLastValue="0" name="commentaire"/>
    <field reuseLastValue="0" name="commune"/>
    <field reuseLastValue="0" name="contact_prestataire"/>
    <field reuseLastValue="0" name="date_debut"/>
    <field reuseLastValue="0" name="date_fin"/>
    <field reuseLastValue="0" name="geom_l"/>
    <field reuseLastValue="0" name="geom_p"/>
    <field reuseLastValue="0" name="geom_s"/>
    <field reuseLastValue="0" name="id"/>
    <field reuseLastValue="0" name="insee_com"/>
    <field reuseLastValue="0" name="nom_site"/>
    <field reuseLastValue="0" name="paturage_nb_jours"/>
    <field reuseLastValue="0" name="paturage_nb_ugb"/>
    <field reuseLastValue="0" name="pg_user"/>
    <field reuseLastValue="0" name="prestation_remuneree"/>
    <field reuseLastValue="0" name="type"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <layerGeometryType>1</layerGeometryType>
</qgis>
